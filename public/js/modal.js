setInterval(function() {
	localStorage.setItem("name", document.querySelector("#name").value);
	localStorage.setItem("email", document.querySelector("#email").value);
	localStorage.setItem("message", document.querySelector("#message").value);
}, 500);

updatestate = function () {
  	$("#modal").modal("hide");
};
$(window).on('popstate', updatestate);

$(document).ready(function(){
  $("#button").click(function(){
  	history.pushState({ page: 2 }, "form", "?form");
	$("#name").val(localStorage.getItem("name"));
	$("#email").val(localStorage.getItem("email"));
	$("#message").val(localStorage.getItem("message"));
    $("#modal").modal("show");
  });
  $("#close").click(function() {
  	history.replaceState({ page: 2 }, "default", "?");
  	$("#modal").modal("hide");
  });
  $("#submit").click(function () {
  localStorage.clear();
  $("#name").val("");
  $("#email").val("");
  $("#message").val("");
});
});
